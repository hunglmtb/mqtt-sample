MQTT_HOST=104.196.24.70

ACCESS_TOKEN=send pm (username)

publish: 'v1/devices/me/attributes' sample {"firmware_version":"1.0.2.lazico", "serial_number":"SN-002"}

publish: 'v1/devices/me/telemetry' sample {"status":"on", "schedule":"on", "active": false}

subscrible 'v1/devices/me/attributes' username:ACCESS_TOKEN


## Getting Started

```bash
git clone https://gitlab.com/hunglmtb/mqtt-sample.git
```

### Run your project


```bash
cd mqtt-sample

npm install

# update config then run , on windows
mqtt-js.bat
```


